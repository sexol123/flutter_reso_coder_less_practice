import 'dart:async';

import 'counter_event.dart';

class CounterBlock {
  int _counter = 0;

  final _counterStateController = StreamController<int>();
  StreamSink<int> get _inCounter => _counterStateController.sink;

  Stream<int> get counter => _counterStateController.stream;

  final _counterEventController = StreamController<CounterEvent>();
  Sink<CounterEvent> get counterEventSink => _counterEventController.sink;

  CounterBlock() {
    _counterEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(CounterEvent event) {
    if (event is IncrementEvent) {
      _counter++;
      _inCounter.add(_counter);
    } else if (event is DecrementEvent) {
      _counter--;
      _inCounter.add(_counter);
    } else
      throw ArgumentError('Unknown Event');
  }

  void dispose() {
    _counterEventController.close();
    _counterStateController.close();
  }
}
